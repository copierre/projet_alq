import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule} from '@angular/common/http';
import {Candy} from './candy';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class CandyService {

  baseUrl = 'http://localhost/api/';

  constructor(private http: HttpClient) { }  

  getAll() {
    return this.http.get(`${this.baseUrl}candy`).pipe(
      map((res: any) => {
        return res['data'];
      })
    );
  }
}
