import { RouterModule, Routes } from "@angular/router";

import { AccueilComponent } from './accueil';
import { DashboardComponent } from './dashboard';
import { StocksComponent } from './stocks';

const routes: Routes = [
    { path: '', component: AccueilComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'stocks', component: StocksComponent },
  
    { path: '**', redirectTo: '' }
  ];
  
  
export const appRoutingModule = RouterModule.forRoot(routes);