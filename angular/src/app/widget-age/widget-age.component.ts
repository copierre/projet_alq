import { Component, OnInit } from '@angular/core';

import {Age} from '../age';
import { AgeService } from '../age.service';

import * as Highcharts from 'highcharts';

declare var require: any;
const More = require('highcharts/highcharts-more');
More(Highcharts);

import Histogram from 'highcharts/modules/histogram-bellcurve';
import { isNgTemplate } from '@angular/compiler';
Histogram(Highcharts);

const Exporting = require('highcharts/modules/exporting');
Exporting(Highcharts);

const ExportData = require('highcharts/modules/export-data');
ExportData(Highcharts);

const Accessibility = require('highcharts/modules/accessibility');
Accessibility(Highcharts);

var series: any = [];

@Component({
  selector: 'app-widget-age',
  templateUrl: './widget-age.component.html',
  styleUrls: ['./widget-age.component.css']
})

export class WidgetAgeComponent implements OnInit {
  
    public data: Age[] = [];   
    options: any;  

    constructor(private DbAge: AgeService) {
    }   
    
    ngOnInit() {
      this.widgetAge();
    }
          

    widgetAge(): void {
      // on récupère les données dans la base de données
      this.DbAge.getAll().subscribe((data: Age[]) => {
        this.data = data;
        for (let i=0; i<data.length; i++) {
          series.push([Number(data[i].age), Number(data[i].nombre)]); 
        }
        // on créer le widget
        this.createWidget(series);
        // on l'affiche
        Highcharts.chart('age', this.options);
      });
    }


    createWidget(series:any): void {     
      this.options = {
        chart: {
          type: 'column'
        },

        title: {
            text: 'Age des clients'
        },

        yAxis: {
          min: 0,
          title: {
              text: 'Nombre'
          }
      },
    
        plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0
            }
        },

        series: [{
          name: 'Clients',
          data: series
        }]
      };
    }
  
}


