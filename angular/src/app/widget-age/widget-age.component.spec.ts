import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetAgeComponent } from './widget-age.component';

describe('WidgetAgeComponent', () => {
  let component: WidgetAgeComponent;
  let fixture: ComponentFixture<WidgetAgeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetAgeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetAgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
