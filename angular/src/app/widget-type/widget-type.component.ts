import { Component, OnInit } from '@angular/core';

import {Type} from '../type';
import { TypeService } from '../type.service';

import * as Highcharts from 'highcharts';

declare var require: any;
const More = require('highcharts/highcharts-more');
More(Highcharts);

import Histogram from 'highcharts/modules/histogram-bellcurve';
import { isNgTemplate } from '@angular/compiler';
Histogram(Highcharts);

const Exporting = require('highcharts/modules/exporting');
Exporting(Highcharts);

const ExportData = require('highcharts/modules/export-data');
ExportData(Highcharts);

const Accessibility = require('highcharts/modules/accessibility');
Accessibility(Highcharts);

var series: any = [];

@Component({
  selector: 'app-widget-type',
  templateUrl: './widget-type.component.html',
  styleUrls: ['./widget-type.component.css']
})

export class WidgetTypeComponent implements OnInit {
  
    public data: Type[] = [];   
    options: any;  

    constructor(private DbType: TypeService) {
    }   
    
    ngOnInit() {
      this.widgetType();
    }
          

    widgetType(): void {
      // on récupère les données dans la base de données
      this.DbType.getAll().subscribe((data: Type[]) => {
        this.data = data;
        for (let i=0; i<data.length; i++) {
          series.push([data[i].type, Number(data[i].nombre)]); 
        }
        // on créer le widget
        this.createWidget(series);
        // on l'affiche
        Highcharts.chart('type', this.options);
      });
    }


    createWidget(series:any): void {   
      this.options = {
        chart: {
          type: 'pie'
        },

        title: {
            text: 'Type des bonbons'
        },
        
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },

        series: [{
          name: 'Types',
          data: series
        }]
      };
    }
  
}


