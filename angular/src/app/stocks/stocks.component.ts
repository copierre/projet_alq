import { Component, OnInit } from '@angular/core';

import {Candy} from '../candy';
import { CandyService } from '../candy.service';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {
  public candy: Candy[] = [];
    

    constructor(private DbCandy: CandyService) {
    }
          
    ngOnInit() {
      this.getCandy();
    }
          
    getCandy(): void {
      this.DbCandy.getAll().subscribe((data: Candy[]) => {
        this.candy = data;
      });
    }
}
