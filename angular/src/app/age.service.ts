import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule} from '@angular/common/http';
import {Age} from './age';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class AgeService {

  baseUrl = 'http://localhost/api/';

  constructor(private http: HttpClient) { }  

  getAll() {
    return this.http.get(`${this.baseUrl}age`).pipe(
      map((res: any) => {
        return res['data'];
      })
    );
  }
}
