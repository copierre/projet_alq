export interface Candy {
    id_candy: number;
    name: string;
    brand: string;
    type: string;
    stock: number;
}