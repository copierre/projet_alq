import { Component, OnInit } from '@angular/core';

import {Marque} from '../marque';
import { MarqueService } from '../marque.service';

import * as Highcharts from 'highcharts';

declare var require: any;
const More = require('highcharts/highcharts-more');
More(Highcharts);

import Histogram from 'highcharts/modules/histogram-bellcurve';
import { isNgTemplate } from '@angular/compiler';
Histogram(Highcharts);

const Exporting = require('highcharts/modules/exporting');
Exporting(Highcharts);

const ExportData = require('highcharts/modules/export-data');
ExportData(Highcharts);

const Accessibility = require('highcharts/modules/accessibility');
Accessibility(Highcharts);

var series: any = [];

@Component({
  selector: 'app-widget-marque',
  templateUrl: './widget-marque.component.html',
  styleUrls: ['./widget-marque.component.css']
})

export class WidgetMarqueComponent implements OnInit {
  
    public data: Marque[] = [];   
    options: any;  

    constructor(private DbMarque: MarqueService) {
    }   
    
    ngOnInit() {
      this.widgetMarque();
    }
          

    widgetMarque(): void {
      // on récupère les données dans la base de données
      this.DbMarque.getAll().subscribe((data: Marque[]) => {
        this.data = data;
        for (let i=0; i<data.length; i++) {
          series.push([data[i].marque, Number(data[i].nombre)]); 
        }
        // on créer le widget
        this.createWidget(series);
        // on l'affiche
        Highcharts.chart('marque', this.options);
      });
    }


    createWidget(series:any): void {   
      this.options = {
        chart: {
          type: 'pie'
        },

        title: {
            text: 'Marque des bonbons'
        },
        
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },

        series: [{
          name: 'Marques',
          data: series
        }]
      };
    }
  
}


