import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetMarqueComponent } from './widget-marque.component';

describe('WidgetMarqueComponent', () => {
  let component: WidgetMarqueComponent;
  let fixture: ComponentFixture<WidgetMarqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetMarqueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetMarqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
