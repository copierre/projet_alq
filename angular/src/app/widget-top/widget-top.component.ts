import { Component, OnInit } from '@angular/core';

import {Top} from '../top';
import { TopService } from '../top.service';

@Component({
  selector: 'app-widget-top',
  templateUrl: './widget-top.component.html',
  styleUrls: ['./widget-top.component.css']
})
export class WidgetTopComponent implements OnInit {
  public top: Top[] = [];
    

    constructor(private DbTop: TopService) {
    }
          
    ngOnInit() {
      this.getTop();
    }
          
    getTop(): void {
      this.DbTop.getAll().subscribe((data: Top[]) => {
        this.top = data;
        console.log(data);
      });
    }
}
