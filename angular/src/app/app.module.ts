import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { appRoutingModule } from './app.routing';
import { RouterModule, Routes } from "@angular/router";
import { AppComponent } from './app.component';
import { EnTeteComponent } from './en-tete/en-tete.component';
import { BoutonsComponent } from './boutons/boutons.component';
import { AccueilComponent } from './accueil/accueil.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { StocksComponent } from './stocks/stocks.component';
import { WidgetAgeComponent } from './widget-age/widget-age.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { WidgetMarqueComponent } from './widget-marque/widget-marque.component';
import { WidgetTypeComponent } from './widget-type/widget-type.component';
import { WidgetTopComponent } from './widget-top/widget-top.component';

const appRoutes: Routes = [];

@NgModule({
  declarations: [
    AppComponent,
    EnTeteComponent,
    BoutonsComponent,
    AccueilComponent,
    DashboardComponent,
    StocksComponent,
    WidgetAgeComponent,
    WidgetMarqueComponent,
    WidgetTypeComponent,
    WidgetTopComponent
  ],
  imports: [
    BrowserModule,
    appRoutingModule,
    HighchartsChartModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes, {
      enableTracing: false,
      onSameUrlNavigation: "reload"})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
