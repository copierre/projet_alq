import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule} from '@angular/common/http';
import {Top} from './top';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class TopService {

  baseUrl = 'http://localhost/api/';

  constructor(private http: HttpClient) { }  

  getAll() {
    return this.http.get(`${this.baseUrl}top`).pipe(
      map((res: any) => {
        return res['data'];
      })
    );
  }
}
