import { TestBed } from '@angular/core/testing';

import { CandyService } from './candy.service';

describe('StocksService', () => {
  let service: CandyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CandyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
