<?php
    /**
     * Marque
     */
    require 'connect.php';
        
    $marque = [];
    $sql = "SELECT brand, COUNT(id_candy) FROM candy GROUP BY brand";

    if($result = mysqli_query($con,$sql))
    {
    $cr = 0;
    while($row = mysqli_fetch_assoc($result))
    {
        $marque[$cr]['marque'] = $row['brand'];
        $marque[$cr]['nombre'] = $row['COUNT(id_candy)'];
        $cr++;
    }
        
    echo json_encode(['data'=>$marque]);
    }
    else
    {
    http_response_code(404);
    }
?>