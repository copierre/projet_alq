<?php
    /**
     * Top 5 des bonbons
     */
    require 'connect.php';
        
    $top = [];
    $sql = "SELECT name, sum(quantity) FROM candy NATURAL JOIN commande_line GROUP BY id_candy ORDER BY sum(quantity) DESC LIMIT 5";

    if($result = mysqli_query($con,$sql))
    {
    $cr = 0;
    while($row = mysqli_fetch_assoc($result))
    {
        $top[$cr]['nom'] = $row['name'];
        $top[$cr]['nombre'] = $row['sum(quantity)'];
        $cr++;
    }
        
    echo json_encode(['data'=>$top]);
    }
    else
    {
    http_response_code(404);
    }
?>