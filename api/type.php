<?php
    /**
     * Type des bonbons
     */
    require 'connect.php';
        
    $type = [];
    $sql = "SELECT type, COUNT(id_candy) FROM candy GROUP BY type";

    if($result = mysqli_query($con,$sql))
    {
    $cr = 0;
    while($row = mysqli_fetch_assoc($result))
    {
        $type[$cr]['type'] = $row['type'];
        $type[$cr]['nombre'] = $row['COUNT(id_candy)'];
        $cr++;
    }
        
    echo json_encode(['data'=>$type]);
    }
    else
    {
    http_response_code(404);
    }
?>