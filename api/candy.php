<?php
    /**
     * Bombons
     */
    require 'connect.php';
        
    $candy = [];
    $sql = "SELECT id_candy, name, brand, type, stock FROM candy";

    if($result = mysqli_query($con,$sql))
    {
    $cr = 0;
    while($row = mysqli_fetch_assoc($result))
    {
        $candy[$cr]['id_candy'] = $row['id_candy'];
        $candy[$cr]['name'] = $row['name'];
        $candy[$cr]['brand'] = $row['brand'];
        $candy[$cr]['type'] = $row['type'];
        $candy[$cr]['stock'] = $row['stock'];
        $cr++;
    }
        
    echo json_encode(['data'=>$candy]);
    }
    else
    {
    http_response_code(404);
    }
?>