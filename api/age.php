<?php
    /**
     * Age
     */
    require 'connect.php';
        
    $age = [];
    $sql = "SELECT age, COUNT(id_client) FROM client GROUP BY age";

    if($result = mysqli_query($con,$sql))
    {
    $cr = 0;
    while($row = mysqli_fetch_assoc($result))
    {
        $age[$cr]['age'] = $row['age'];
        $age[$cr]['nombre'] = $row['COUNT(id_client)'];
        $cr++;
    }
        
    echo json_encode(['data'=>$age]);
    }
    else
    {
    http_response_code(404);
    }
?>